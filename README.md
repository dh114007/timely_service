
<h1 align="center"> TIMELY客服系统</h1> 

<p align="center">    
    <b>如果对您有帮助，您可以点右上角 "Star" 支持一下 谢谢！</b>
</p>
<h2 align="center">禁止将本项目的代码和资源进行任何形式的出售，禁止用于含有木马、病毒、色情、赌博、诈骗等违法违规业务。产生的一切任何后果责任由侵权者自负。</h2> 

## 项目介绍
   TIMELY客服系统是基于ThinkPhp5.1+Layui+Swoole开发的一套在线客服系统。
### 导航栏目
 | [官网地址](http://www.hongpua.com/)
 | [Layui开发手册](https://www.layui.com/doc/)
 | [TP5.1开发手册](https://www.kancloud.cn/manual/thinkphp5_1/353946)
 | [服务器](https://www.aliyun.com/minisite/goods?userCode=eiw2xsig)

####   :fire:  后台:  

![add image](https://gitee.com/zhc02/timely_service/raw/master/public/static/common/images/hou.jpg)

添加客服成功之后  登录客服工作平台

![add image](https://gitee.com/zhc02/timely_service/raw/master/public/static/common/images/kefuh.jpg)

### Timely推荐阿里云服务器配置
 入门级配置
 ```
 CPU：1核
 内存：1G
 实例规格：突发性能t5实例
 带宽：1M
 系统：CentOS 7.4 64位(推荐)
 价格：366元/年 933.3元/三年
 ```
 <a href="https://www.aliyun.com/minisite/goods?userCode=eiw2xsig">学生9.5/月</a>
   </td>
 <td>
 
 标准级配置
 ```
 CPU：2核
 内存：4G
 实例规格：突发性能t5实例
 带宽：2M
 系统：CentOS 7.4 64位(推荐)
 价格：936元/年 2386.8元/三年
 ```
 <a href="https://www.aliyun.com/minisite/goods?userCode=eiw2xsig">新用户半价</a>
 
   </td>
   <td>
 
 企业级配置
 ```
 CPU：4核
 内存：8G
 实例规格：突发性能t6实例
 带宽：5M
 系统：CentOS 7.4 64位(推荐)
 价格：2786.64元/年 5389.20元/三年
 ```
 <a href="https://www.aliyun.com/minisite/goods?userCode=eiw2xsig">领取￥2000红包</a>
 
   </td>
   </tr></table>


### 开源版使用须知
1.允许用于个人学习、毕业设计、教学案例、公益事业;

2.如果商用必须保留版权信息，请自觉遵守。开源版不适合商用，商用请购买商业版;

3.禁止将本项目的代码和资源进行任何形式的出售，禁止用于含有木马、病毒、色情、赌博、诈骗等违法违规业务。产生的一切任何后果责任由侵权者自负。


### 商户版本：

如需申请商业版试用  请登录官网联系作者开通账号试用。

客服工作台

![add image](https://gitee.com/zhc02/timely_service/raw/master/public/static/common/images/pc.png)

用户端

![add image](https://gitee.com/zhc02/timely_service/raw/master/public/static/common/images/h5.png)


#### 安装教程

1.  下载本代码
2.  需要php7.2版本以上
3.  需要swoole4.3.2版本以上

#### 使用说明

1.   导入sql文件，修改config/databases.php,并搭建好nginx站点
2.   在项目根目录运行 mkdir -R 777 runtime
3.   在项目根目录运行php think chat start   支持 start | start -d |restart |stop
4.    然后浏览器访问 www.xxxx.com/index/kefu/index



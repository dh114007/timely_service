CREATE TABLE `#__admin`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账号',
  `password` varchar(155) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录密码',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic COMMENT='后台用户表';;


CREATE TABLE `#__chat_log`  (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `from_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发送者id',
  `from_name` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发送者名称',
  `from_avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发送者头像',
  `to_id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接收方id',
  `to_name` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接受者名称',
  `to_avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接收者头像',
  `message` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发送的内容',
  `send_status` tinyint(1) NULL DEFAULT 1 COMMENT '发送状态 1发送成功  2发送失败   可以重发',
  `read_flag` tinyint(1) NULL DEFAULT 1 COMMENT '是否已读 1 未读 2 已读',
  `create_time` datetime NOT NULL COMMENT '记录时间',
  PRIMARY KEY (`log_id`) USING BTREE,
  INDEX `from_id`(`from_id`) USING BTREE,
  INDEX `to_id`(`to_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic COMMENT='聊天记录表';


CREATE TABLE `#__kefu_info`  (
  `kefu_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '客服id',
  `kefu_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客服唯一标识',
  `kefu_name` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客服名称',
  `kefu_avatar` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客服头像',
  `kefu_password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客服密码',
  `online_status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '在线状态 1 在线 2 离线',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `client_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客服登录标示',
  PRIMARY KEY (`kefu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic COMMENT='客服信息表';


CREATE TABLE `#__visitor`  (
  `vid` int(11) NOT NULL AUTO_INCREMENT,
  `visitor_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客id',
  `visitor_name` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客名称',
  `visitor_avatar` varchar(155) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客头像',
  `visitor_ip` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '访客ip',
  `client_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客户端标识',
  `online_status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0 离线 1 在线',
  `create_time` datetime NOT NULL COMMENT '访问时间',
  PRIMARY KEY (`vid`) USING BTREE,
  INDEX `visiter`(`visitor_id`) USING BTREE,
  INDEX `time`(`create_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic COMMENT='访客信息表';

CREATE TABLE `#__visitor_queue`  (
  `qid` int(11) NOT NULL AUTO_INCREMENT COMMENT '队列id',
  `visitor_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客id',
  `visitor_name` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客名称',
  `visitor_avatar` varchar(155) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客头像',
  `visitor_ip` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '访客ip',
  `client_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客户端标识',
  `create_time` datetime NOT NULL COMMENT '访问时间',
  `reception_status` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '接待状态  0 等待接待中 1 接待中  2接待完成',
  `kefu_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `kefu_client_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`qid`) USING BTREE,
  UNIQUE INDEX `id`(`visitor_id`) USING BTREE,
  INDEX `visiter`(`visitor_id`) USING BTREE,
  INDEX `time`(`create_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic COMMENT='聊天等待表';


CREATE TABLE `#__visitor_service_log`  (
  `vsid` int(11) NOT NULL AUTO_INCREMENT COMMENT '服务编号',
  `visitor_id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客id',
  `client_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客的客户端标识',
  `visitor_name` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客名称',
  `visitor_avatar` varchar(155) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客头像',
  `visitor_ip` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客的ip',
  `kefu_id` int(11) NOT NULL,
  `kefu_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '接待的客服标识',
  `kefu_name` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客服名称',
  `start_date` datetime NOT NULL COMMENT '开始服务时间',
  `end_date` datetime NULL DEFAULT NULL COMMENT '结束服务时间',
  `connect_stauts` tinyint(3) NOT NULL DEFAULT 1 COMMENT '连接状态  1 正在连接  2 关闭连接',
  PRIMARY KEY (`vsid`) USING BTREE,
  INDEX `user_id,client_id`(`visitor_id`, `client_id`) USING BTREE,
  INDEX `kf_id,start_time,end_time`(`kefu_code`, `start_date`, `end_date`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic COMMENT='服务日志表';





